# image de base
FROM httpd:alpine

# Expose le port 3000 du conteneur pour le rendre accessible à l'hôte et aux autres conteneurs docker.
EXPOSE 80

COPY http/htdocs/index.html /usr/local/apache2/htdocs/

CMD ["httpd", "-D", "FOREGROUND"]