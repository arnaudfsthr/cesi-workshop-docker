# Comment lancer le conteneur docker

```
docker run --rm -p 80:80 -v /Users/arnaudfeisthauer/Desktop/cesi-workshop-docker/http/htdocs/:/usr/local/apache2/htdocs httpd:alpine
```

```
docker build -t httpd:custom .
docker run --rm -p 80:80 httpd:custom
```